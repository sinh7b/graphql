const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const connectToDb = async () => {
    return await mongoose.connect('mongodb://localhost:27017/GrapQLDemo', { useNewUrlParser: true } );
};

module.exports = connectToDb;