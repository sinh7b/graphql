const { combineResolvers } = require('graphql-resolvers');
const messages = require('../models/message');
const users = require('../models/user');
const { isAuthentication, isMessageOwner } = require('./authorization');

module.exports = {
    Query: {
        message: async (parent, { id }) => {
            const Messages = await messages.findById(id).lean(true);
            
            return Messages;
        },
        messages: async () => {
            const Messages = await messages.find().lean(true);  
            return Object.values(Messages);
        },
    },

    Mutation: {
        createMessage: combineResolvers(isAuthentication, 
            async (parent, { content }, { me }) => {
                const userId = me._id;
                const newMessage = new messages({
                    content,
                    userId,
                });
                await newMessage.save();
    
                return newMessage;
            },    
        ),
        
        deleteMessage: combineResolvers(
            isAuthentication,
            isMessageOwner,
            async (parent, { id }) => {
                await messages.findByIdAndDelete(id);
                return true;
            },
        ),
    },

    Message: {
        id: (parent) => {
            return parent._id;
        }
    }
};