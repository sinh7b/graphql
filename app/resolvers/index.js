const userResolver = require('./user');
const messageResolver = require('./message');
const { users, messages } = require('../models');


module.exports = [userResolver, messageResolver];