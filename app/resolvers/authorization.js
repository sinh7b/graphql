const { ForbiddenError } = require('apollo-server');
const { skip } = require('graphql-resolvers');
const messages = require('../models/message');

module.exports.isAuthentication = (parent, args, { me }) => me ? skip : new ForbiddenError('Not authenticated as user');

module.exports.isMessageOwner = async (parent, { id }, { me }) => 
{
    const message = await messages.findById(id).lean(true);
    if (message.userId != me._id) {
        return new ForbiddenError('Not authentication as owner!');
    }

    return skip;
};

module.exports.isAdmin = async (parent, args, { me }) => (me.role === "ADMIN") ? skip : new ForbiddenError('You are not a admin!');
