const uuidv4 = require('uuid/v4');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { combineResolvers } = require('graphql-resolvers');
const { isAuthentication, isAdmin } = require('./authorization');
const users = require('../models/user');
const messages = require('../models/message');

const createToken = async (user, secret, expiresIn) => {
    const { _id, username, password, role } = user;
    return await jwt.sign({ _id, username, password, role }, secret, { expiresIn });
}   


module.exports = {
    Query: {
        user: async (parent, { id }) => {
            const user = await users.findById(id).lean(true);
            return user;
        },
        users: async () => {
            const user = await users.find().lean(true);
            return Object.values(user);
        },
        me: (parent, args, { me }) => me,
    },

    Mutation: {
        createUser: async (parent, { username }) => {
            const newUser = new users({
                username
            });
            await newUser.save();
            return newUser;
        },

        signUp: async (
        parent,
        { username, password },
        { secret }
        ) => {
            const hash = await bcrypt.hash(password, 10);
            const newUser = new users({
                username,
                password: hash,
            });
            await newUser.save();
            
            return { token: createToken(newUser, secret, '30m') };
        },

        login: async (parent, { username, password }, { secret }) => {
            const user = await users.findOne({ username }).lean(true);
            if (!user) {
                throw new Error('User not found!');
            }
            const isCorrectPassword = await bcrypt.compare(password, user.password);
            if (isCorrectPassword === false) {
                throw new Error('Password is not correct!');
            }

            return { token: createToken(user, secret, '30m') };
        },

        deleteUser: combineResolvers( 
            isAuthentication,
            isAdmin,
            async (parent, { id }) => {
                await users.findByIdAndDelete(id);
                return true;
            },
        ),
    },

    User: {
        username: (parent) => {
            return parent.username;
        },
        id: (parent) => {
            return parent._id;
        },
        messageIds: async (parent) => {
            const messageArray = await messages.find({ userId: parent._id }).lean(true);
            return Object.values(messageArray);
        },
    },
};