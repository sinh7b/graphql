const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const MessageSchema = new Schema({
    content: {
        type: String,
        required: true,
    },
    userId: {
        type: ObjectId,
        ref: 'user',
        required: true,
    },
}, { timestamps: true });

module.exports = mongoose.model('message', MessageSchema);