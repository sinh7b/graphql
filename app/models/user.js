const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: [true, 'Username is required'],
        maxlength: [50, 'Username is to long!'],
        minlength: [8, 'Username is too short!']
    },
    password: String,
    role: {
        type: String,
        default: ''
    }
});

module.exports = mongoose.model('user', UserSchema);