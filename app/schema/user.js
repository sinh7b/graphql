const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        user(id: ID!): User!
        users: [User!]
        me: User!
    }

    extend type Mutation {
        createUser(username: String!): User!
        signUp(
        username: String!
        password: String!
        ): Token!
        login(
            username: String!
            password: String!
        ): Token!
        deleteUser(
            id: ID!
        ): Boolean!
    }

    type Token {
        token: String!
    }

    type User {
        id: ID!
        username: String!
        messageIds: [Message!]
        role: String!
    }

`;