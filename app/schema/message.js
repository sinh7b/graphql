const { gql } = require('apollo-server-express');


module.exports = gql`
    extend type Query {
        message(id: ID!): Message!
        messages: [Message!]
    }

    extend type Mutation {
        createMessage(content: String!): Message!
        deleteMessage(id: ID!): Boolean!
    }

    type Message {
        id: ID!
        content: String!
        userId: ID!
    }
`;