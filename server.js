const express = require('express');
const { ApolloServer, AuthenticationError } = require('apollo-server-express');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const schema = require('./app/schema');
const resolvers = require('./app/resolvers');
const { secret } = require('./app/config');

const connectDB = require('./app/db');

const port = 8000;

const app = express();  

connectDB().then(() => console.log('Connected db')).catch(e => {
	console.error(e);
	process.exit();
});

app.use(cors());

const getMe = async (req) => {
    const token = req.headers['x-token'];
    if (token) {
        try {
            return await jwt.verify(token, secret);
        } catch (error) {
            throw new AuthenticationError('Sign in again!!');
        }
    }
} 


const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
    context: async ({ req }) => {
        const me = await getMe(req);
        return {
            me,
            secret,
        };
    },
});

server.applyMiddleware({ app, path: '/graphql' });

app.listen({ port }, () => {
    console.log(`Server is on port ${ port }`);
});
